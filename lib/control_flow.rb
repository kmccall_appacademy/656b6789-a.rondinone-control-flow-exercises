# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |letter|
    if letter == letter.downcase
      str.delete!(letter)
    end
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  half_way = str.length/2

  if str.length % 2 == 0
    return str[half_way - 1] + str[half_way]
  elsif str.length % 2 == 1
    return str[half_way]
  end

end

# Return the number of vowels in a string.

VOWELS = %w(a e i o u)

def num_vowels(str)
  vowel_count = 0
  str.each_char do |letter|
    if VOWELS.include?(letter)
      vowel_count += 1
    end
  end
  vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  i = 1
  while i <= num
    product *= i
    i += 1
  end
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
   join_str = ""

   i = 0
   while i < arr.length
     word = arr[i]
     if i != arr.length - 1
       join_str += word
       join_str += separator
     else
       join_str += word
     end
     i += 1
   end
   join_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_str = ""
  i = 0
  while i < str.length
    letter = str[i]
    if i % 2 == 0
      new_str += letter.downcase
    elsif i % 2 == 1
      new_str += letter.upcase
    end
    i += 1
  end
  new_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(" ")
  result = []

  words.each do |word|
    if word.length >= 5
      result << word.reverse
    else
      result << word
    end
  end
  result.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []

  i = 1
  while i <= n
    if i % 3 == 0 && i % 5 == 0
      result << "fizzbuzz"
    elsif i % 5 == 0
      result << "buzz"
    elsif i % 3 == 0
      result << "fizz"
    else
      result << i
    end
    i += 1
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
  i = arr.length - 1
  while i >= 0
    ele = arr[i]
    new_arr << ele
    i -= 1
  end
  new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2

  i = 2
  while i < num
    if num % i == 0
      return false
    end
    i += 1
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  i = 1
  while i <= num
    if num % i == 0
      result << i
    end
    i += 1
  end
  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  result = []
  i = 1
  while i <= num
    if num % i == 0 && prime?(i)
      result << i
    end
    i += 1
  end
  result
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  primes = prime_factors(num)
  primes.count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = []
  even = []

  arr.each do |i|
    if i.odd?
      odd << i
      if odd.length > 1
        return even[0]
      end
    else
      even << i
      if even.length > 1
        return odd[0]
      end
    end
    end
end
